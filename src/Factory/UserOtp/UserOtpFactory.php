<?php

declare(strict_types=1);

namespace App\Factory\UserOtp;

use App\Entity\User;
use App\Entity\UserOtp;

class UserOtpFactory
{
    public function create(User $user): UserOtp
    {
        return (new UserOtp())
            ->setOtp((string) rand(1000000, 9999999))
            ->setUser($user)
            ->setCreatedAt(new \DateTime())
            ->setExpiresAt((new \DateTime())->modify('+20 minutes'))
            ->setIsActive(true);
    }
}
