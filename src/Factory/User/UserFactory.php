<?php

declare(strict_types=1);

namespace App\Factory\User;

use App\DTO\User\Request\CreateUserRequest;
use App\Entity\User;

class UserFactory
{
    public function create(CreateUserRequest $createUserRequest): User
    {
        return (new User())
            ->setFirstName($createUserRequest->getFirstName())
            ->setLastName($createUserRequest->getLastName())
            ->setPhoneNumber($createUserRequest->getPhoneNumber())
            ->setEmail($createUserRequest->getEmail());
    }
}
