<?php

declare(strict_types=1);

namespace App\Command;

use App\DTO\User\Request\CreateUserRequest;
use App\Service\User\UserCreator;
use App\Validator\NonExistingEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterUserCommand extends Command
{
    protected static $defaultName = 'user:register';

    /** @var Question */
    private $firstNameQuestion;

    /** @var Question */
    private $lastNameQuestion;

    /** @var Question */
    private $emailQuestion;

    /** @var Question */
    private $phoneQuestion;

    /** @var UserCreator */
    private $userCreator;

    /** @var ValidatorInterface */
    private $validator;

    public function __construct(UserCreator $userCreator, ValidatorInterface $validator)
    {
        $this->userCreator = $userCreator;
        $this->validator = $validator;

        $this->firstNameQuestion = new Question('Please enter the user\'s first name: ', null);
        $this->lastNameQuestion = new Question('Please enter the user\'s last name: ', null);
        $this->emailQuestion = new Question('Please enter the user\'s email: ', null);
        $this->phoneQuestion = new Question('Please enter the user\'s phone number: ', null);

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a user. If you did not enter the inputs, you will be asked to enter them interactively.')
            ->addArgument('firstName', InputArgument::OPTIONAL, 'The user\'s first name')
            ->addArgument('lastName', InputArgument::OPTIONAL, 'The user\'s last name')
            ->addArgument('phoneNumber', InputArgument::OPTIONAL, 'The user\'s phone number')
            ->addArgument('email', InputArgument::OPTIONAL, 'The user\'s email');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $firstName = $input->getArgument('firstName');
        $lastName = $input->getArgument('lastName');
        $phoneNumber = $input->getArgument('phoneNumber');
        $email = $input->getArgument('email');

        $helper = $this->getHelper('question');

        while (is_null($firstName) || is_null($lastName) || is_null($phoneNumber)) {
            $output->writeln([
                '',
                'User Creator Interactive Mode',
                '=============================',
                '',
            ]);

            $firstName = is_null($firstName) ? $helper->ask($input, $output, $this->firstNameQuestion) : $firstName;
            $lastName = is_null($lastName) ? $helper->ask($input, $output, $this->lastNameQuestion) : $lastName;
            $phoneNumber = is_null($phoneNumber) ? $helper->ask($input, $output, $this->phoneQuestion) : $phoneNumber;
        }

        while (is_null($email) || !$this->isValidEmailAddress($email)) {
            $email = $helper->ask($input, $output, $this->emailQuestion);
            $output->writeln('The email has an invalid format or already exists');
        }

        $createUserRequest = new CreateUserRequest($firstName, $lastName, $phoneNumber, $email);
        $user = $this->userCreator->create($createUserRequest);

        $output->writeln(
            sprintf('%s %s has been registered successfully, ID: %s', $user->getFirstName(), $user->getLastName(), $user->getId())
        );
    }

    private function isValidEmailAddress(string $email): bool
    {
        $constraints = [new Email(), new NonExistingEmail()];

        return 0 === $this->validator->validate($email, $constraints)->count();
    }
}
