<?php

declare(strict_types=1);

namespace App\Service\User;

use App\DTO\User\Request\CreateUserRequest;
use App\Entity\User;
use App\Factory\User\UserFactory;
use Doctrine\ORM\EntityManagerInterface;

class UserCreator
{
    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserFactory $userFactory, EntityManagerInterface $entityManager)
    {
        $this->userFactory = $userFactory;
        $this->entityManager = $entityManager;
    }

    public function create(CreateUserRequest $createUserRequest): User
    {
        $user = $this->userFactory->create($createUserRequest);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
