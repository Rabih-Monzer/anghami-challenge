<?php

declare(strict_types=1);

namespace App\Service\UserSession;

use App\Dictionary\UserAuthenticationStatusDictionary;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserSessionManager
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function setUserSessionByAuthenticationStatus(User $user, string $authenticationStatus)
    {
        $this->session->set($user->getEmail(), $authenticationStatus);
    }

    public function getCurrentUserSessionStatus(User $user): string
    {
        $userSessionStatus = $this->session->get($user->getEmail());

        return is_null($userSessionStatus) ? UserAuthenticationStatusDictionary::ANONYMOUS_USER : $userSessionStatus;
    }

    public function removeUserSession(User $user): void
    {
        $this->session->remove($user->getEmail());
    }

    public function hasAuthenticatedUserSession(User $user): bool
    {
        return UserAuthenticationStatusDictionary::AUTHENTICATED_USER === $this->getCurrentUserSessionStatus($user);
    }
}
