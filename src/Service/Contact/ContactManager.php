<?php

declare(strict_types=1);

namespace App\Service\Contact;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;

class ContactManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function softDeleteTeamMember(Contact $contact): void
    {
        $contact->setDeletedAt(new \DateTime());
        $this->saveContactData($contact);
    }

    public function saveContactData(Contact $contact): void
    {
        $this->entityManager->persist($contact);
        $this->entityManager->flush();
    }
}
