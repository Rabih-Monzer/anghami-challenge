<?php

declare(strict_types=1);

namespace App\Service\UserOtp;

use App\Entity\User;
use App\Repository\UserOtpRepository;
use Doctrine\ORM\EntityManagerInterface;

class OtpDeactivator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserOtpRepository
     */
    private $userOtpRepository;

    public function __construct(EntityManagerInterface $entityManager, UserOtpRepository $userOtpRepository)
    {
        $this->entityManager = $entityManager;
        $this->userOtpRepository = $userOtpRepository;
    }

    public function deactivateActiveOtp(User $user): void
    {
        $userOtp = $this->userOtpRepository->findOneBy(['user' => $user, 'isActive' => true]);

        if (!is_null($userOtp)) {
            $userOtp->setIsActive(false);

            $this->entityManager->persist($userOtp);
            $this->entityManager->flush();
        }
    }
}
