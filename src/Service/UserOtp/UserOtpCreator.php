<?php

declare(strict_types=1);

namespace App\Service\UserOtp;

use App\Entity\User;
use App\Event\UserOtp\UserOtpCreatedEvent;
use App\Factory\UserOtp\UserOtpFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserOtpCreator
{
    /**
     * @var UserOtpFactory
     */
    private $userOtpFactory;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(UserOtpFactory $userOtpFactory, EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->userOtpFactory = $userOtpFactory;
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function create(User $user): void
    {
        $userOtp = $this->userOtpFactory->create($user);

        $this->em->persist($userOtp);
        $this->em->flush();

        $this->eventDispatcher->dispatch(new UserOtpCreatedEvent($userOtp));
    }
}
