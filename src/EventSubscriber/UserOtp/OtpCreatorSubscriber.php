<?php

namespace App\EventSubscriber\UserOtp;

use App\Event\User\UserLoggedInEvent;
use App\Service\UserOtp\OtpDeactivator;
use App\Service\UserOtp\UserOtpCreator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OtpCreatorSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserOtpCreator
     */
    private $userOtpCreator;

    /**
     * @var OtpDeactivator
     */
    private $otpDeactivator;

    public function __construct(UserOtpCreator $userOtpCreator, OtpDeactivator $otpDeactivator)
    {
        $this->userOtpCreator = $userOtpCreator;
        $this->otpDeactivator = $otpDeactivator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserLoggedInEvent::class => 'onUserLoggedInEvent',
        ];
    }

    public function onUserLoggedInEvent(UserLoggedInEvent $userLoggedInEvent)
    {
        $this->otpDeactivator->deactivateActiveOtp($userLoggedInEvent->getUser());
        $this->userOtpCreator->create($userLoggedInEvent->getUser());
    }
}
