<?php

declare(strict_types=1);

namespace App\EventSubscriber\UserOtp;

use App\Entity\UserOtp;
use App\Event\UserOtp\UserOtpCreatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class OtpSenderSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserOtpCreatedEvent::class => 'onUserOtpCreatedEvent',
        ];
    }

    public function onUserOtpCreatedEvent(UserOtpCreatedEvent $otpCreatedEvent)
    {
        $userOtp = $otpCreatedEvent->getUserOtp();
        $email = $this->generateUserOtpTemplatedEmail($userOtp);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(
                sprintf(
                    'Error while sending user OTP Email: userEmail: %s, OTP: %s having error: %s',
                    $userOtp->getUser()->getEmail(),
                    $userOtp->getOtp(),
                    $e->getMessage()
                )
            );
        }
    }

    private function generateUserOtpTemplatedEmail(UserOtp $userOtp): TemplatedEmail
    {
        return (new TemplatedEmail())
            ->from('rabih.s.monzer@gmail.com')
            ->to($userOtp->getUser()->getEmail())
            ->subject('Address Book - OTP Code')
            ->htmlTemplate('emails/user-otp.html.twig')
            ->context(['userOtp' => $userOtp]);
    }
}
