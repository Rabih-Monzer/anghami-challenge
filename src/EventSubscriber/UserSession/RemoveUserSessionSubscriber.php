<?php

declare(strict_types=1);

namespace App\EventSubscriber\UserSession;

use App\Event\User\UserLoggedOutEvent;
use App\Service\UserSession\UserSessionManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RemoveUserSessionSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    public function __construct(UserSessionManager $userSessionManager)
    {
        $this->userSessionManager = $userSessionManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserLoggedOutEvent::class => 'onUserLoggedOutEvent',
        ];
    }

    public function onUserLoggedOutEvent(UserLoggedOutEvent $userLoggedOutEvent)
    {
        $this->userSessionManager->removeUserSession($userLoggedOutEvent->getUser());
    }
}
