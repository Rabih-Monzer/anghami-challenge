<?php

declare(strict_types=1);

namespace App\EventSubscriber\UserSession;

use App\Dictionary\UserAuthenticationStatusDictionary;
use App\Event\UserOtp\OtpValidatedEvent;
use App\Service\UserSession\UserSessionManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AuthenticateUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    public function __construct(UserSessionManager $userSessionManager)
    {
        $this->userSessionManager = $userSessionManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            OtpValidatedEvent::class => 'onOtpValidatedEvent',
        ];
    }

    public function onOtpValidatedEvent(OtpValidatedEvent $otpValidatedEvent)
    {
        $this->userSessionManager->setUserSessionByAuthenticationStatus(
            $otpValidatedEvent->getUserOtp()->getUser(),
            UserAuthenticationStatusDictionary::AUTHENTICATED_USER
        );
    }
}
