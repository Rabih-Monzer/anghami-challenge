<?php

namespace App\EventSubscriber\UserSession;

use App\Dictionary\UserAuthenticationStatusDictionary;
use App\Event\User\UserLoggedInEvent;
use App\Service\UserSession\UserSessionManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IdentifyUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    public function __construct(UserSessionManager $userSessionManager)
    {
        $this->userSessionManager = $userSessionManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserLoggedInEvent::class => 'onUserLoggedInEvent',
        ];
    }

    public function onUserLoggedInEvent(UserLoggedInEvent $userLoggedInEvent)
    {
        $this->userSessionManager->setUserSessionByAuthenticationStatus(
            $userLoggedInEvent->getUser(),
            UserAuthenticationStatusDictionary::IDENTIFIED_USER
        );
    }
}
