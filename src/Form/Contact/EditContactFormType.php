<?php

declare(strict_types=1);

namespace App\Form\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['constraints' => [new NotNull(), new NotBlank()]])
            ->add('lastName', TextType::class, ['constraints' => [new NotNull(), new NotBlank()]])
            ->add('email', EmailType::class, ['required' => false])
            ->add('phoneNumber', TextType::class, ['required' => false])
            ->add('jobTitle', TextType::class, ['required' => false])
            ->add('imageFile', VichImageType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('button', SubmitType::class, ['label' => 'Update Contact']);
    }
}
