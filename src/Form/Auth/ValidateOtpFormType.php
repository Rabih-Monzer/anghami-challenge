<?php

namespace App\Form\Auth;

use App\Validator\OtpSubmission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ValidateOtpFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('otp', IntegerType::class, ['constraints' => [new NotNull(), new NotBlank(), new OtpSubmission()]])
            ->add('submit', SubmitType::class, ['label' => 'Submit OTP']);
    }
}
