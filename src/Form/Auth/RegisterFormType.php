<?php

namespace App\Form\Auth;

use App\Entity\User;
use App\Validator\NonExistingEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['constraints' => [new NotBlank(), new NotNull()]])
            ->add('lastName', TextType::class, ['constraints' => [new NotBlank(), new NotNull()]])
            ->add('email', EmailType::class, ['constraints' => [new NotNull(), new NotBlank(), new NonExistingEmail()]])
            ->add('phoneNumber', TelType::class)
            ->add('save', SubmitType::class, ['label' => 'Register']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
