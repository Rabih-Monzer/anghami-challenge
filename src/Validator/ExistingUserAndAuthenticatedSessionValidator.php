<?php

declare(strict_types=1);

namespace App\Validator;

use App\Dictionary\UserAuthenticationStatusDictionary;
use App\Repository\UserRepository;
use App\Service\UserSession\UserSessionManager;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExistingUserAndAuthenticatedSessionValidator extends ConstraintValidator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    public function __construct(UserRepository $userRepository, UserSessionManager $userSessionManager)
    {
        $this->userRepository = $userRepository;
        $this->userSessionManager = $userSessionManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ExistingUserAndAuthenticatedSession) {
            throw new UnexpectedTypeException($constraint, ExistingUserAndAuthenticatedSession::class);
        }

        $user = is_null($value) ? null : $this->userRepository->find($value);

        if (is_null($user) || UserAuthenticationStatusDictionary::AUTHENTICATED_USER !== $this->userSessionManager->getCurrentUserSessionStatus($user)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
