<?php

declare(strict_types=1);

namespace App\Validator;

use App\Repository\UserRepository;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NonExistingEmailValidator extends ConstraintValidator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NonExistingEmail) {
            throw new UnexpectedTypeException($constraint, NonExistingEmail::class);
        }

        if (!is_null($this->userRepository->findOneBy(['email' => $value]))) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
