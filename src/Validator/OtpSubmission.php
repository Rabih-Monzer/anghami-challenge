<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OtpSubmission extends Constraint
{
    public $message = '{{ value }}';
}
