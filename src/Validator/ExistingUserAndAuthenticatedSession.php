<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExistingUserAndAuthenticatedSession extends Constraint
{
    public $message = 'You do not have an authenticated session.';
}
