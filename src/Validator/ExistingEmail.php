<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExistingEmail extends Constraint
{
    public $message = 'This email does not exist.';
}
