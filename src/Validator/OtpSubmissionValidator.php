<?php

namespace App\Validator;

use App\Dictionary\UserAuthenticationStatusDictionary;
use App\Repository\UserOtpRepository;
use App\Service\UserSession\UserSessionManager;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OtpSubmissionValidator extends ConstraintValidator
{
    public const INVALID_OTP_MESSAGE = 'The OTP you entered is invalid, expired, or inactive. Please log in again';
    public const INVALID_USER_SESSION_MESSAGE = 'Your session has expired. Please log in again';

    /**
     * @var UserOtpRepository
     */
    private $userOtpRepository;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    public function __construct(UserOtpRepository $userOtpRepository, UserSessionManager $userSessionManager)
    {
        $this->userOtpRepository = $userOtpRepository;
        $this->userSessionManager = $userSessionManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof OtpSubmission) {
            throw new UnexpectedTypeException($constraint, OtpSubmission::class);
        }

        $userOtp = $this->userOtpRepository->findOneByActiveAndUnexpired($value);

        if (is_null($userOtp)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', self::INVALID_OTP_MESSAGE)
                ->addViolation();

            return;
        }

        // User should have an identified session before changing him to authenticated
        if (UserAuthenticationStatusDictionary::IDENTIFIED_USER !== $this->userSessionManager->getCurrentUserSessionStatus($userOtp->getUser())) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', self::INVALID_USER_SESSION_MESSAGE)
                ->addViolation();
        }
    }
}
