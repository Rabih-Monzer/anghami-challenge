<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NonExistingEmail extends Constraint
{
    public $message = 'The email is already registered';
}
