<?php

declare(strict_types=1);

namespace App\Event\UserOtp;

use App\Entity\UserOtp;
use Symfony\Contracts\EventDispatcher\Event;

class OtpValidatedEvent extends Event
{
    public const NAME = 'userOtp.validated';

    /**
     * @var UserOtp
     */
    private $userOtp;

    public function __construct(UserOtp $userOtp)
    {
        $this->userOtp = $userOtp;
    }

    public function getUserOtp(): UserOtp
    {
        return $this->userOtp;
    }
}
