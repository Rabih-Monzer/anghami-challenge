<?php

declare(strict_types=1);

namespace App\Dictionary;

class UserAuthenticationStatusDictionary
{
    public const ANONYMOUS_USER = 'Anonymous User';
    public const IDENTIFIED_USER = 'Identified User';
    public const AUTHENTICATED_USER = 'Authenticated User';
}
