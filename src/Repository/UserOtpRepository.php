<?php

namespace App\Repository;

use App\Entity\UserOtp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserOtp|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOtp|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOtp[]    findAll()
 * @method UserOtp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOtpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserOtp::class);
    }

    public function findOneByActiveAndUnexpired(int $otp): ?UserOtp
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.otp = :otp')
            ->setParameter('otp', $otp)
            ->andWhere('u.isActive = 1')
            ->andWhere('u.expiresAt > :currentDateAndTime')
            ->setParameter('currentDateAndTime', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
