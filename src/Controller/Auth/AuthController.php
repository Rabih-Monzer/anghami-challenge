<?php

namespace App\Controller\Auth;

use App\Entity\User;
use App\Event\User\UserLoggedInEvent;
use App\Event\User\UserLoggedOutEvent;
use App\Event\UserOtp\OtpValidatedEvent;
use App\Form\Auth\LoginFormType;
use App\Form\Auth\RegisterFormType;
use App\Form\Auth\ValidateOtpFormType;
use App\Repository\UserOtpRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var UserOtpRepository
     */
    private $userOtpRepository;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        EventDispatcherInterface $eventDispatcher,
        UserOtpRepository $userOtpRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->userOtpRepository = $userOtpRepository;
    }

    /**
     * @Route("/register", name="register_form")
     */
    public function register(Request $request): Response
    {
        $registerForm = $this->createForm(RegisterFormType::class);

        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $this->em->persist($registerForm->getData());
            $this->em->flush();

            return $this->redirectToRoute('login_form');
        }

        return $this->render('auth/register.html.twig', [
            'registerForm' => $registerForm->createView(),
        ]);
    }

    /**
     * @Route("/", name="login_form")
     */
    public function login(Request $request): Response
    {
        $loginForm = $this->createForm(LoginFormType::class);

        $loginForm->handleRequest($request);

        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $user = $this->userRepository->findOneBy(['email' => $loginForm->getData()['email']]);
            $this->eventDispatcher->dispatch(new UserLoggedInEvent($user));

            return $this->redirectToRoute('validate-otp');
        }

        return $this->render('auth/login.html.twig', [
            'loginForm' => $loginForm->createView(),
        ]);
    }

    /**
     * @Route("/validate-otp", name="validate-otp")
     */
    public function validateOtp(Request $request): Response
    {
        $validateOtpForm = $this->createForm(ValidateOtpFormType::class);

        $validateOtpForm->handleRequest($request);

        if ($validateOtpForm->isSubmitted() && $validateOtpForm->isValid()) {
            $userOtp = $this->userOtpRepository->findOneBy(['otp' => $validateOtpForm->getData()['otp']]);

            $this->eventDispatcher->dispatch(new OtpValidatedEvent($userOtp));

            return $this->redirectToRoute('contacts-list', ['userId' => $userOtp->getUser()->getId()]);
        }

        return $this->render('auth/validateOtp.html.twig', [
            'validateOtpForm' => $validateOtpForm->createView(),
        ]);
    }

    /**
     * @Route("/logout/{user}", name="logout", methods={"POST"})
     */
    public function logout(Request $request, User $user): Response
    {
        $submittedToken = $request->request->get('token');

        if (!$this->isCsrfTokenValid('user_logout', $submittedToken)) {
            return new Response('CSRF token is not valid', Response::HTTP_FORBIDDEN);
        }

        $this->eventDispatcher->dispatch(new UserLoggedOutEvent($user));

        return $this->redirectToRoute('login_form');
    }
}
