<?php

declare(strict_types=1);

namespace App\Controller\Contact;

use App\Entity\Contact;
use App\Entity\User;
use App\Form\Contact\CreateContactFormType;
use App\Form\Contact\EditContactFormType;
use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use App\Service\Contact\ContactManager;
use App\Service\UserSession\UserSessionManager;
use App\Validator\ExistingUserAndAuthenticatedSession;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactsController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    /**
     * @var ContactManager
     */
    private $contactManager;

    public function __construct(
        UserRepository $userRepository,
        ContactRepository $contactRepository,
        UserSessionManager $userSessionManager,
        ContactManager $contactManager
    ) {
        $this->userRepository = $userRepository;
        $this->contactRepository = $contactRepository;
        $this->userSessionManager = $userSessionManager;
        $this->contactManager = $contactManager;
    }

    /**
     * @Rest\QueryParam(
     *     name="userId",
     *     strict=true,
     *     allowBlank=false,
     *     requirements=@ExistingUserAndAuthenticatedSession,
     *     description="Requestor id"
     * )
     *
     * @Route("/contacts", name="contacts-list")
     */
    public function index(ParamFetcherInterface $paramFetcher): Response
    {
        $user = $this->userRepository->find($paramFetcher->get('userId'));
        $contacts = $this->contactRepository->findNonDeletedContactsByUser($user);

        return $this->render('contacts/contacts-list.html.twig', [
            'contacts' => $contacts,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/contacts/{contact}", name="contact-delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contact $contact): Response
    {
        $user = $contact->getUser();
        $submittedToken = $request->request->get('token');

        if (!$this->isCsrfTokenValid('delete-contact', $submittedToken)) {
            return new Response('CSRF token is not valid', Response::HTTP_FORBIDDEN);
        }

        if (!$this->userSessionManager->hasAuthenticatedUserSession($user)) {
            return new Response('You should be authenticated to perform this action', Response::HTTP_FORBIDDEN);
        }

        $this->contactManager->softDeleteTeamMember($contact);

        return $this->redirectToRoute('contacts-list', [
            'userId' => $user->getId(),
        ]);
    }

    /**
     * @Rest\QueryParam(name="userId",
     *     strict=true,
     *     allowBlank=false,
     *     requirements=@ExistingUserAndAuthenticatedSession,
     *     description="Requestor id"
     * )
     *
     * @Route("/contacts/create", name="contact-create")
     */
    public function createNewContact(Request $request, ParamFetcherInterface $paramFetcher): Response
    {
        $user = $this->userRepository->find($paramFetcher->get('userId'));

        $createContactForm = $this->createForm(CreateContactFormType::class);
        $createContactForm->handleRequest($request);

        if ($createContactForm->isSubmitted() && $createContactForm->isValid()) {
            $this->handleContactData($createContactForm, $user);

            return $this->redirectToRoute('contacts-list', ['userId' => $user->getId()]);
        }

        return $this->render('contacts/contact-create.html.twig', [
            'createContactForm' => $createContactForm->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/contacts/{contact}/edit", name="contact-edit")
     */
    public function editContact(Request $request, Contact $contact): Response
    {
        if (!$this->userSessionManager->hasAuthenticatedUserSession($contact->getUser())) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $editContactForm = $this->createForm(EditContactFormType::class, $contact);
        $editContactForm->handleRequest($request);

        if ($editContactForm->isSubmitted() && $editContactForm->isValid()) {
            $contact = $editContactForm->getData();
            $this->contactManager->saveContactData($contact);
            $this->addFlash('success', 'Contact Updated Successfully');

            return $this->redirectToRoute('contacts-list', ['userId' => $contact->getUser()->getId()]);
        }

        return $this->render('contacts/contact-edit.html.twig', [
            'editContactForm' => $editContactForm->createView(),
            'user' => $contact->getUser(),
        ]);
    }

    private function handleContactData(FormInterface $createContactForm, User $user): void
    {
        $contact = $createContactForm->getData();
        $contact->setUser($user);

        $this->contactManager->saveContactData($contact);
        $this->addFlash('success', 'Contact Added Successfully');
    }
}
