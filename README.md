# Installation

- You should have Composer and MYSQL installed on your local machine.
- If you want to recieve OTP emails, I should verify your email on AWS SES (Just send me an email on rabih.s.monzer@gmail.com and i'll verify your email (OR you can get the OTP from the database))
#### NOTES:
Those commands have been tested on a linux machine

#### Clone the repository.
```sh
git clone git@bitbucket.org:Rabih-Monzer/anghami-challenge.git
```

#### Create a new database for the project (default is called AddressBook but you can change the configurations as u wish).
```sh
CREATE DATABASE AddressBook;
```

#### Install Dependencies.

```sh
cd anghami-challenge
composer install
```

#### Run Migrations.

```sh
./bin/console doctrine:migrations:migrate
```

#### Run Local Server
```sh
./bin/console server:run
```

#### Open Your Browser
```sh
http://127.0.0.1:8000
```

